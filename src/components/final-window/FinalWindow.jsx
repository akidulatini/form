import { useContext } from 'react'
import finalWindowStyles from './FinalWindow.module.css'
import { AppContext } from '../../context/AppContext'
import { BASE_URL } from '../../constants'

const deliveryOptions = {
    'courier': 'Курьер',
    'express': 'Экcпресс',
    'cargo': 'Грузовой',
    'cargocorp': 'Грузовой корпоративный' 
}

const FinalWindow = () => {
    const ctx = useContext(AppContext)

    const handleSubmit = () => {
        const data = {
            items: [],
            taxi_class: ctx.delivery.taxi_class,
            from_address: ctx.delivery.from_address,
            order: ctx.order
        }
        data.items = ctx.items.map(item => {
            return {
                height: item.size.height,
                length: item.size.length,
                width: item.size.width,
                quantity: item.quantity,
                item_cost: item.cost_value
            }
        })
        fetch(`${BASE_URL}/orders/order_deliveries/`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        })
            .then(res => res.json())
            .then(data => window.location.href = `https://api.cable.kz/admin/orders/orderdelivery/${data.order_delivery.id}/change/`)
    }

    return (
        <div className={finalWindowStyles.blocks}>
            <p className={finalWindowStyles.total}>Итог</p>
            { ctx && ctx.items.map((item, index) => {
                return (
                    <div key={index} className={finalWindowStyles.block}>
                        <div key={index} className={finalWindowStyles.item}>
                            <p>Длина</p>
                            <p>{item.size.length} м</p>
                        </div>
                        <div className={finalWindowStyles.item}>
                            <p>Ширина</p>
                            <p>{item.size.width} м</p>
                        </div>
                        <div className={finalWindowStyles.item}>
                            <p>Высота</p>
                            <p>{item.size.height} м</p>
                        </div>
                        <div className={finalWindowStyles.item}>
                            <p>Цена</p>
                            <p>{item.cost_value} тг</p>
                        </div>
                        <div className={finalWindowStyles.item}>
                            <p>Кол-во</p>
                            <p>{item.quantity}</p>
                        </div>
                    </div>
                )
            })}


            <div className={finalWindowStyles.bottom}>
                <div className={finalWindowStyles.delivery}>
                    <p>Откуда</p>
                    <p>{ctx.delivery.from_address}</p>
                </div>
                <div className={finalWindowStyles.delivery}>
                    <p>Способ доставки</p>
                    <p>{deliveryOptions[ctx.delivery.taxi_class]}</p>
                </div>
                <div className={finalWindowStyles.totalPrice}>
                    <p>Итог стоимости</p>
                    <p>{ctx.delivery.price} тг</p>
                </div>
            </div>
            <button 
                type='button' 
                className={finalWindowStyles.button}
                onClick={handleSubmit}
            >
                Подтвердить
            </button>
        </div>
    )
}

export default FinalWindow