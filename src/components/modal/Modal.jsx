import { useCallback, useEffect, useRef, useState } from 'react';
import { Portal, createContainer } from '../../portal/Portal';
import modalStyles from './Modal.module.css'

const MODAL_CONTAINER_ID = 'modal-container-id';

// eslint-disable-next-line react/prop-types
const Modal = ({ onClose, title, children }) => {
  const [isMounted, setMounted] = useState(false);
  const rootRef = useRef(null)

  const handleClose = useCallback(() => {
    onClose?.()
  }, [onClose])
  
  useEffect(() => {
    createContainer({ id: MODAL_CONTAINER_ID });
    setMounted(true);
  }, []);

  useEffect(() => {
    const handleWrapperClick = e => {
      const { target } = e;

      if (target instanceof Node && rootRef.current === target) {
        onClose?.();
      }
    };

    const handleEscapePress = e => {
      if (e.key === 'Escape') {
        onClose?.();
      }
    };

    window.addEventListener('click', handleWrapperClick);
    window.addEventListener('keydown', handleEscapePress);

    return () => {
      window.removeEventListener('click', handleWrapperClick);
      window.removeEventListener('keydown', handleEscapePress);
    };
  }, [onClose]);

  return (
    isMounted
    ? (
        <Portal id={MODAL_CONTAINER_ID}>
            <div className={modalStyles.wrap} ref={rootRef}>
                <div className={modalStyles.content}>
                    <p className={modalStyles.title}>{title}</p>
                    {children}

                    <button
                        type="button"
                        className={modalStyles.closeButton}
                        onClick={handleClose}
                    >
                        x
                    </button>
                </div>
            </div>
        </Portal>
    )
    : null
  );
};

export default Modal;