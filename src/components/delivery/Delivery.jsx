import { useContext } from 'react'
import deliveryStyles from './Delivery.module.css'
import { useHistory } from 'react-router-dom'
import { AppContext } from '../../context/AppContext'

const deliveryOptions = {
    'courier': 'Курьер',
    'express': 'Экcпресс',
    'cargo': 'Грузовой',
    'cargocorp': 'Грузовой корпоративный' 
}

const Delivery = () => {
    const history = useHistory()
    const ctx = useContext(AppContext)

    const handleButton = () => {
        history.push('/final')
    }

    return (
        <div className={deliveryStyles.form}>
            <h2 className={deliveryStyles.heading}>Тип доставки</h2>
            <div className={deliveryStyles.select}>
                <p>{deliveryOptions[ctx.delivery.taxi_class]}</p>
                <p>{ctx.delivery.price} тг</p>
            </div>
            <button 
                onClick={handleButton} 
                type='button' 
                className={deliveryStyles.button}
            >
                Далее
            </button>
        </div>
    )
}

export default Delivery