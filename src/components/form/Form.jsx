import { useContext, useEffect, useRef, useState } from 'react'
import Dimensions from '../dimensions/Dimensions'
import formStyles from './Form.module.css'
import { useHistory, useLocation } from 'react-router-dom'
import { v4 as uuidv4 } from 'uuid';
import { AppContext } from '../../context/AppContext';
import { BASE_URL } from '../../constants';
import LoadingSpinner from '../../ui/loader/Loader'
import Modal from '../modal/Modal';
import { FullscreenControl, Map, SearchControl } from 'react-yandex-maps';
import { Placemark } from 'react-yandex-maps';

const Form = () => {
    const [sizes, setSizes] = useState([
        { id: uuidv4() }
    ])
    const [values, setValues] = useState([])
    const [address, setAddress] = useState('')
    const [error, setError] = useState('')
    const [isLoading, setIsLoading] = useState(false)
    const [isSubmitting, setIsSubmitting] = useState(false);
    const [isModalActive, setIsModalActive] = useState(false)
    const [searchValue, setSearchValue] = useState('')
    const [searchOptions, setSearchOptions] = useState([])
    const [selectedLocation, setSelectedLocation] = useState(null);
    const searchRef = useRef(null)
    const mapRef = useRef(null)
    const history = useHistory()
    const location = useLocation()
    const queryParams = new URLSearchParams(location.search);
    const id = queryParams.get('id')
    const ctx = useContext(AppContext)
    ctx.order = id

    const handleModalClose = () => setIsModalActive(false)
    
    const handleSubmit = e => {
        e.preventDefault()
        setIsLoading(true)
        setIsSubmitting(true)
        ctx.items = values.map(item => {
            return {
                cost_value: parseInt(item.price.current),
                quantity: parseFloat(item.count.current),
                size: {
                    length: parseFloat(item.length.current),
                    width: parseFloat(item.width.current),
                    height: parseFloat(item.height.current),
                }
            }
        })
        ctx.delivery.from_address = address
        const data = {
            order_id: ctx.order,
            from_address: address,
            items: [
                ...ctx.items
            ]
        }
        fetch(`${BASE_URL}/orders/orders/yandex_check_price/`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        })
            .then(res => res.json())
            .then(data => {
                if (data.message) {
                    setError(data.message)
                    setIsLoading(false)
                    setIsModalActive(true)
                } else {
                    ctx.delivery.taxi_class = data.requirements.taxi_class
                    ctx.delivery.price = data.price
                    history.push('/delivery')
                }
            })
            .catch(error => {
                setError(error)
                setIsLoading(false)
                setIsModalActive(true)
            })
    }

    const handleAddButton = () => {
        setSizes([
            ...sizes, 
            { id: uuidv4() }
        ])
    }

    const handleDelete = (id) => {
        setSizes(sizes.filter(item => item.id !== id))
    }

    const handleSearchMap = (value) => {
        searchRef.current.search(value).then((result) => {
          const resultsArray = result.geoObjects.toArray();
          const locationsArray = resultsArray.map((geoObject) => {
            const name = geoObject.properties.get("name");
            const city = geoObject.properties.get("description");
            const coordinates = geoObject.geometry.getCoordinates(); 
            setAddress(geoObject.properties.get("text"))
            return { name, city, coordinates };
          });
          setSearchOptions([...locationsArray]);
        });
    };

    const handleSearchOutsideClick = (event) => {
        if (searchOptions.length > 0 && !event.target.closest(`.${formStyles.searchOptions}`)) {
          setSearchOptions([]);
        }
    };

    const handleLocationSelection = (location) => {
        setSelectedLocation(location)
        mapRef.current.setCenter(location.coordinates, 16);
    }
    
    useEffect(() => {
        document.body.addEventListener('mousedown', handleSearchOutsideClick);
        return () => {
            document.body.removeEventListener('mousedown', handleSearchOutsideClick);
        };
    }, [searchOptions]);      

    return (
        <form className={formStyles.form}>
            { isModalActive && <Modal onClose={handleModalClose}>{error && error}</Modal>}
            {!isLoading && (
                <>
                <p className={formStyles.note}>Максимальные размеры: <br/> длина - 4м, ширина - 1.9м, высота - 2м</p>
                {sizes.length ? (
                    sizes.map((item) => (
                    <Dimensions
                        key={item.id}
                        handleDelete={handleDelete}
                        handleSubmit={isSubmitting}
                        patchValues={setValues}
                        id={item.id}
                    />
                    ))
                ) : (
                    <p className={formStyles.hint}>Добавьте блок с измерениями</p>
                )}

                <button
                    onClick={handleAddButton}
                    type="button"
                    className={formStyles.addButton}
                >
                    <span>Добавить</span>
                    <span className={formStyles.plus}>+</span>
                </button>

                <div className={formStyles.address}>
                    <label className={formStyles.label} htmlFor="address">
                        Адрес
                    </label>
                    <input
                        className={formStyles.input}
                        type="text"
                        name="address"
                        placeholder="Адрес откуда будете отправлять"
                        value={searchValue}
                        onChange={(e) => setSearchValue(e.target.value)}
                    />
                    {searchOptions.length > 0 && (
                        <ul className={formStyles.searchOptions}>
                            {searchOptions.map((item, index) => (
                                <li 
                                    className={formStyles.searchOptions__item} 
                                    key={index}
                                    onClick={() => {
                                        handleLocationSelection(item)
                                        setSearchOptions([])
                                    }}
                                >
                                    <p>{item.name}</p>
                                    <p>{item.city}</p>
                                </li>
                            ))}
                        </ul>
                    )}
                </div>

                <button
                    onClick={() => handleSearchMap(searchValue)}
                    type="button"
                    className={formStyles.searchButton}
                    disabled={!searchValue}
                >
                    Поиск
                </button>

                <div className={formStyles.map}>
                    <Map 
                        defaultState={{ 
                            center: [43.24, 76.92], 
                            zoom: 11,
                            controls: []
                        }} 
                        width='100%'
                        height='100%'
                        instanceRef={ref => {
                            if (ref) mapRef.current = ref;
                        }}
                    >
                        {selectedLocation && (
                            <Placemark 
                                geometry={selectedLocation.coordinates}
                                options={{
                                    preset: "islands#redIcon"
                                }}
                            />
                        )}
                        <SearchControl 
                            instanceRef={ref => {
                                if (ref) searchRef.current = ref;
                            }}
                        />
                        <FullscreenControl />
                    </Map>
                </div>

                <button
                    onClick={handleSubmit}
                    type="submit"
                    className={formStyles.button}
                    disabled={!sizes.length}
                >
                    Далее
                </button>
                </>
            )}

            {isLoading && <LoadingSpinner />}
        </form>

    )
}

export default Form
