/* eslint-disable react/prop-types */
import { MAX_HEIGHT, MAX_LENGTH, MAX_WIDTH } from '../../constants'
import dimensionsStyles from './Dimensions.module.css'
import { useState, useEffect, useRef } from 'react'

const Dimensions = (props) => {
    const {
        handleDelete, 
        id, 
        isSubmitting, 
        patchValues,
    } = props

    let [length, setLength] = useState("")
    let [width, setWidth] = useState("")
    let [height, setHeight] = useState("")
    let [price, setPrice] = useState("")
    let [count, setCount] = useState(0)
    const [error, setError] = useState('')

    let lengthRef = useRef("")
    let widthRef = useRef("")
    let heightRef = useRef("")
    let priceRef = useRef("")
    let countRef = useRef(0)

    useEffect(() => {
        patchValues((prev) => [...prev, 
            {
                length: lengthRef, 
                width: widthRef, 
                height: heightRef, 
                price: priceRef,
                count: countRef
            }
        ])
    }, [isSubmitting])

    const handleIncrement = () => {
        setCount(++count)
        countRef.current += 1
    }

    const handleDecrement = () => {
        if (count > 0) {
            setCount(--count)
            countRef.current -= 1
        }
    }

    return (
        <div className={dimensionsStyles.container}>
            <div className={dimensionsStyles.item}>
                <label className={dimensionsStyles.label} htmlFor="length">Длина</label>
                <input 
                    className={dimensionsStyles.input} 
                    type="number" 
                    step="0"
                    name="length"
                    placeholder='0.45м'
                    value={length}
                    onChange={(e) => {
                        const { value } = e.target
                        lengthRef.current = value
                        if (value < 0) {
                            return
                        } else if (value > MAX_LENGTH) {
                            setError('Длина не может быть больше 4м')
                        } else {
                            setError('')
                            setLength(value)
                        }
                    }}
                />
                <p className={dimensionsStyles.error}>{error && error}</p>
            </div>
            <div className={dimensionsStyles.item}>
                <label className={dimensionsStyles.label} htmlFor="width">Ширина</label>
                <input
                    className={dimensionsStyles.input}
                    type="number" 
                    step="0"
                    name="width"
                    placeholder='0.45м'
                    value={width}
                    onChange={(e) => {
                        const { value } = e.target
                        widthRef.current = value
                        if (value < 0) {
                            return
                        } else if (value > MAX_WIDTH) {
                            setError('Ширина не может быть больше 1.9м')
                        } else {
                            setError('')
                            setWidth(value)
                        }
                    }}
                />
            </div>
            <div className={dimensionsStyles.item}>
                <label className={dimensionsStyles.label} htmlFor="height">Высота</label>
                <input
                    className={dimensionsStyles.input}
                    type="number" 
                    step='0'
                    name="height"
                    placeholder='0.45м'
                    value={height}
                    onChange={(e) => {
                        const { value } = e.target
                        heightRef.current = value
                        if (value < 0) {
                            return
                        } else if (value > MAX_HEIGHT) {
                            setError('Высота не может быть больше 2м')
                        } else {
                            setError('')
                            setHeight(value)
                        }
                    }}
                />
            </div>
            <div className={dimensionsStyles.item}>
                <label className={dimensionsStyles.label} htmlFor="price">Цена</label>
                <input
                    className={dimensionsStyles.input}
                    type="number" 
                    name="price"
                    placeholder='Введите цену одной упаковки'
                    value={price}
                    onChange={(e) => {
                        const { value } = e.target
                        priceRef.current = value
                        if (value < 0) return
                        setPrice(value)
                    }}
                />
            </div>
            <div className={dimensionsStyles.item}>
                <p className={dimensionsStyles.label}>Кол-во</p>
                <div className={dimensionsStyles.counter}>
                    <button type='button' onClick={handleDecrement} className={dimensionsStyles.math}>-</button>
                    <p className={dimensionsStyles.number}>{ count }</p>
                    <button type='button' onClick={handleIncrement} className={dimensionsStyles.math}>+</button>
                </div>
            </div>
            <button onClick={() => handleDelete(id)} className={dimensionsStyles.close} type='button'></button>
        </div>
    )
}

export default Dimensions
