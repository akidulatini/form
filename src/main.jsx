import ReactDOM from 'react-dom';
import './styles/reset.css'
import './styles/normalize.css'
import App from './components/app/App.jsx'
import { BrowserRouter as Router } from "react-router-dom";
import AppProvider from './context/AppContext'
import { YMaps } from 'react-yandex-maps';
import { YANDEX_MAPS_API_KEY } from './constants';

ReactDOM.render(
  <Router>
    {/* <React.StrictMode> */}
    <AppProvider>
      <YMaps query={{ apikey: YANDEX_MAPS_API_KEY }}>
        <App />
      </YMaps>
    </AppProvider>
    {/* </React.StrictMode> */}
  </Router>
, document.getElementById('root'));

