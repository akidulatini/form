import { createContext } from 'react'

export const AppContext = createContext()

// eslint-disable-next-line react/prop-types
const AppProvider = ({ children }) => {
    const data = {
        items: [],
        delivery: {
            taxi_class: '',
            price: 0,
            from_address: ''
        },
        order: null
    }

    return (
        <AppContext.Provider value={ data }>
            { children }
        </AppContext.Provider>
    )
}

export default AppProvider
