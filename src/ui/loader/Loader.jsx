import loadingSpinnerStyles from './Loader.module.css'

const LoadingSpinner = () => {
    return (
        <div className={loadingSpinnerStyles.loaderContainer}>
            <div className={loadingSpinnerStyles.loader}></div>
        </div>
    )
}

export default LoadingSpinner
